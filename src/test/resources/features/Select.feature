Feature: As a customer, I should be able to select one of the bets

  Scenario: I want to bet for first scorer
    Given I want to find "Manchester United Vs Manchester City" matches
    When I want to play "Kevin De Bruyne" will score first
    Then I should see my bet

  Scenario: I should be able to delete my bet
    When I want to find "Manchester United Vs Manchester City" matches
    Then I should delete my bet