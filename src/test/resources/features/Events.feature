Feature: As a customer, I should see both football and tennis bets

  Scenario: Assert that service returns correct number of events
    When I should see the all available bets
    Then I should see that number of events are 4

  Scenario: Assert that service returns both football and tennis bets
    When I should ask available bets
    Then I should see category is "Football" or "Tennis"