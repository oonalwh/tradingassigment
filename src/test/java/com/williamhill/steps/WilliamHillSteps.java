package com.williamhill.steps;

import com.williamhill.domain.entity.Selection;
import com.williamhill.domain.helper.WilliamHillHelper;
import com.williamhill.domain.response.Event;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class WilliamHillSteps {
    private WilliamHillHelper williamHillHelper;
    private List<Event> eventList;
    private Event myEvent;
    private List<Event.Markets.Selections> selectionList;
    private int myBetId;

    public WilliamHillSteps(WilliamHillHelper williamHillHelper) {
        this.williamHillHelper = williamHillHelper;
    }

    @When("^I removed all the tennis bets$")
    public void iRemovedAllTheTennisBets() {
        williamHillHelper.removeAllTennisEvents();
    }

    @Then("^I should see that there is no tennis events$")
    public void iShouldSeeThatThereIsNoTennisEvents() {
        eventList = williamHillHelper.getAllEvents();
        for (Event event : eventList) {
            Assert.assertNotEquals(event.category, "Tennis");
        }
    }

    @When("^I should see the all available bets$")
    public void iShouldSeeTheAllAvailableBets() {
        eventList = williamHillHelper.getAllEvents();
    }

    @Then("^I should see that number of events are (\\d+)$")
    public void iShouldSeeThatNumberOfEventsAre(int numberOfEvents) {
        Assert.assertEquals(eventList.size(), numberOfEvents);
    }

    @When("^I should ask available bets$")
    public void iShouldAskAvailableBets() {
        eventList = williamHillHelper.getAllEvents();
    }

    @Then("^I should see category is \"([^\"]*)\" or \"([^\"]*)\"$")
    public void iShouldSeeCategoryIsOr(String category1, String category2) throws Throwable {
        for (Event event : eventList) {
            Assert.assertTrue(event.category.equals(category1) || event.category.equals(category2));
        }
    }

    @When("^I want to find \"([^\"]*)\" matches$")
    public void iWantToFindMatches(String matchName) throws Throwable {
        eventList = williamHillHelper.getAllEvents();
        for (Event event : eventList) {
            if (event.name.equals(matchName)) {
                myEvent = event;
            }
        }
    }

    @Then("^I want to play \"([^\"]*)\" will score first$")
    public void iWantToPlayWillScoreFirst(String firstGoalScorer) throws Throwable {
        myBetId = williamHillHelper.playBet(firstGoalScorer, myEvent);
    }

    @Then("^I should delete my bet$")
    public void iShouldDeleteMyBet() {
        Assert.assertTrue(williamHillHelper.deleteMyBet(myEvent.id));
    }

    @Then("^I should see my bet$")
    public void iShouldSeeMyBet() {
        selectionList = williamHillHelper.getEvent(myEvent.id).markets.get(1).selections;
        for (Event.Markets.Selections selections : selectionList){
            if (selections.id == myBetId){
                Assert.assertEquals(selections.selection, "Kevin De Bruyne");
                Assert.assertEquals(selections.result, "Won");
            }
        }
    }
}
