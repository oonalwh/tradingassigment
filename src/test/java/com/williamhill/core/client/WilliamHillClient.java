package com.williamhill.core.client;

import com.williamhill.domain.response.Event;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;

import java.util.List;

import static io.restassured.RestAssured.given;

public class WilliamHillClient {

    public WilliamHillClient() {
    }

    protected boolean post(String url, Object body) {
        return given().contentType(ContentType.JSON).body(body).when().post(url).getStatusCode() == HttpStatus.SC_CREATED;
    }

    protected List<Event> getList(String url) {
        return given().when().get(url).then().extract().response().jsonPath().getList("events", Event.class);
    }

    protected Event get(String url) {
        return given().when().get(url).then().extract().response().getBody().jsonPath().getObject("", Event.class);
    }

    protected boolean delete(String url) {
        return given().contentType("application/json").when().delete(url).getStatusCode() == HttpStatus.SC_OK;
    }
}
