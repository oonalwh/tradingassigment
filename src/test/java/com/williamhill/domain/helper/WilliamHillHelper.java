package com.williamhill.domain.helper;

import com.williamhill.domain.builder.SelectionBuilder;
import com.williamhill.domain.client.EventsClient;
import com.williamhill.domain.entity.Selection;
import com.williamhill.domain.response.Event;
import org.junit.Assert;

import java.util.List;

public class WilliamHillHelper {
    private EventsClient eventsClient;
    private List<Event> eventList;
    private Selection selection;

    public WilliamHillHelper(EventsClient eventsClient) {
        this.eventsClient = eventsClient;
    }

    public List<Event> getAllEvents(){
       return eventsClient.getEvents();
    }

    public int playBet(String firstGoalScorer, Event event){
        selection = new SelectionBuilder().withSelection(firstGoalScorer).build();
        Assert.assertTrue(eventsClient.selection(event.id, 122, selection));
        return selection.id;
    }

    public boolean deleteMyBet(int eventId){
         return eventsClient.deleteBet(eventId, 122, 24);
    }

    public Event getEvent(int eventId){
        return eventsClient.getEventWithId(eventId);
    }

    public void removeAllTennisEvents(){
        eventList = eventsClient.getEvents();
        for (Event event : eventList) {
            if (event.category.equals("Tennis")){
                eventsClient.removeEventWithEventId(event.id);
            }
        }
    }
}
