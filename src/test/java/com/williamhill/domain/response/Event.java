package com.williamhill.domain.response;

import java.util.List;

public class Event {
    public int id;
    public String name;
    public String status;
    public String startTime;
    public String category;
    public List<Markets> markets;


    public static class Markets{
        public int id;
        public String name;
        public String status;
        public List<Selections> selections;

        public static class Selections{
            public int id;
            public String selection;
            public String status;
            public double price;
            public String result;
        }
    }
}
