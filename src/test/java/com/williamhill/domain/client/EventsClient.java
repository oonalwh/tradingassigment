package com.williamhill.domain.client;

import com.williamhill.core.client.WilliamHillClient;
import com.williamhill.core.configuration.ConfigParser;
import com.williamhill.domain.entity.Selection;
import com.williamhill.domain.response.Event;

import java.util.List;

public class EventsClient extends WilliamHillClient {
    private String url;

    public EventsClient(){
        this.url = ConfigParser.getValue("BaseUrl");
    }

    public List<Event> getEvents(){
        return getList(url + "/events");
    }

    public Event getEventWithId(int eventId){
        return get(url + "/event/" + eventId);
    }

    public void removeEventWithEventId(int eventId){
         delete(url + "/event/" + eventId);
    }

    public boolean selection(int eventId, int marketId, Selection selection){
        return post(url + "/event/" + eventId + "/market/" + marketId + "/addSelection", selection);
    }

    public boolean deleteBet(int eventId, int marketId, int selectionId){
        return delete(url + "/event/" + eventId + "/market/" + marketId + "/" + selectionId);
    }

}
