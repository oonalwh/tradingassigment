package com.williamhill.domain.entity;

import java.util.List;

public class Event {
    public int id;
    public String name;
    public String status;
    public String startTime;
    public String category;
    public List<Markets> markets;

    public static class Markets{
        public int id;
        public String name;
        public String status;
        public List<Selections> selections;

        public Markets(int id, String name, String status, List<Selections> selections){
            this.id = id;
            this.name = name;
            this.status = status;
            this.selections = selections;
        }

        public static class Selections{
            public int id;
            public String selection;
            public String status;
            public double price;
            public String result;


            public Selections(int id, String selection, String status,double price, String result){
                this.id = id;
                this.selection = selection;
                this.status = status;
                this.price = price;
                this.result = result;
            }
        }
    }
}
