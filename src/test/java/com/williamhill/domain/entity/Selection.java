package com.williamhill.domain.entity;

public class Selection {
    public int id;
    public String selection;
    public int price;
    public SelectionStatus status;
    public ResultStatus result;
}
