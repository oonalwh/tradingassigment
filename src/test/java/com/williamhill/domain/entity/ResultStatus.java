package com.williamhill.domain.entity;

public enum ResultStatus {
    EMPTY,
    Lost,
    Won;
}
