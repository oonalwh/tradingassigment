package com.williamhill.domain.entity;

public enum  SelectionStatus {
    Active,
    Suspended
}
