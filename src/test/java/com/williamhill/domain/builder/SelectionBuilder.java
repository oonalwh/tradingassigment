package com.williamhill.domain.builder;

import com.williamhill.domain.entity.ResultStatus;
import com.williamhill.domain.entity.Selection;
import com.williamhill.domain.entity.SelectionStatus;

public class SelectionBuilder {
    private int _id = 24;
    private String _selection = "Sergio Aguero";
    private int _price = 4;
    private SelectionStatus _status = SelectionStatus.Active;
    private ResultStatus _result = ResultStatus.Won;


    public SelectionBuilder withSelection(String value){
        _selection = value;
        return this;
    }

    public Selection build(){
        return new Selection(){{
            this.id = _id;
            this.selection = _selection;
            this.price = _price;
            this.status = _status;
            this.result = _result;
        }};
    }
}
