# WilliamHill-rest-assured
  Rest Assured API Test Framework

# to run
  mvn test -Dcucumber.options="classpath:features/"


# to see report
    - install allure from here https://docs.qameta.io/allure/#_installing_a_commandline

    When test execution finished, enter below command inside willamhill project

    - allure serve target/surefire-reports/

# to see build log

    - you can see latest builds from travis https://travis-ci.org/ozanberk/Williamhill
